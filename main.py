import sys
import utils.clean_dataframe as ut
import utils.build_folder as bf

def main() -> int:
    folder_path = "./db/"
    list_companies = ut.list_all_companies(folder_path)

    for i in list_companies:
        information_company = ut.extract_company_information(i)
        list_references_10K = ut.get_list_references_10K(folder_path+i)
        list_year_report_date = ut.extract_years_from_timestamps(ut.get_list_dates(folder_path+i))
        zipped = zip(list_references_10K, list_year_report_date)
        merged_years = ut.merge_years_with_references(list(zipped))
        bf.create_folder_structure(merged_years, information_company)
    return 0

if __name__ == '__main__':
    sys.exit(main()) 