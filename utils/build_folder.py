import os
import sys
sys.path.append("./utils")
from scrape import *

def scraping_wrapper(sublinks, path):
    for i in sublinks:
        #scraping('https://www.sec.gov/ix?doc=/Archives/edgar/data/1022344/000155837023001840/spg-20221231x10k.htm', './test')
        #exit(-1)
        scraping(i, path)

def create_folder_structure(data_dict, company_info):
    category, company_name, _ = company_info
    for year, urls in data_dict.items():
        path = f"./companies/{category}/{company_name}/{year}"
        if not os.path.exists(path):
            os.makedirs(path)
        sublinks = []
        for i in urls:
            sublinks = extract_all_sublinks(i)
            scraping_wrapper(sublinks, path)