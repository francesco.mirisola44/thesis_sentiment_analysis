import os
import pandas as pd

#format: industryType-companyName-CIK
def extract_company_information(filename):
    elements = filename.split("-")
    return elements[0], elements[1], elements[-1][:-5]

def extract_years_from_dates(date_list):
    years_list = []
    for date_str in date_list:
        year = date_str.split('-')[0]
        years_list.append(year)
    return years_list

def build_path(industry, company, CIK, year):
    return industry+'/'+company+'('+CIK+')'+'/'+year

def create_folder_if_not_exists(folder_path):
    try:
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
            print(f"Folder '{folder_path}' created successfully.")
        else:
            print(f"Folder '{folder_path}' already exists.")
    except OSError as e:
        print(f"Error while creating folder: {e}")

def list_files_in_folder(folder_path):
    try:
        files = os.listdir(folder_path)
        return files
    except OSError as e:
        print(f"Error: {e}")
        return []

#cleaning dataframe
#param file: filename with full path
def get_10K_links(file):
    df = pd.read_excel(file)
    df = df[df['Form type'].str.contains('10-K')]
    df.to_excel(file, index=False)

def list_all_companies(folder_path):
    return list_files_in_folder(folder_path)

def processing(folder_path, file_list):
    for i in file_list:
        get_10K_links(folder_path+i)

def get_list_dates(filename):
    df = pd.read_excel(filename)
    return df['Reporting date'].to_list()

def get_list_references_10K(filename):
    df = pd.read_excel(filename)
    return df['Filings URL'].to_list()

def extract_years_from_timestamps(timestamp_list):
    years_list = [ts.year for ts in timestamp_list]
    return years_list

def merge_years_with_references(data_list):
    year_htm_mapping = {}
    for htm_ref, year in data_list:
        if year not in year_htm_mapping:
            year_htm_mapping[year] = []
        year_htm_mapping[year].append(htm_ref)

    #merged_data = [(htm_ref, year) for year, htm_refs in year_htm_mapping.items() for htm_ref in htm_refs]
    return year_htm_mapping