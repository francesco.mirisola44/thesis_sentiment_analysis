from selenium import webdriver
from selenium.webdriver.common.by import By
import re
from bs4 import BeautifulSoup
from lxml.html.clean import Cleaner
import os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

def clean_htmll(raw_html):
    content_without_styles = Cleaner(style=True).clean_html(raw_html)
    CLEANR = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')#remove html tags
    clean_text = re.sub(CLEANR, '', content_without_styles)
    clean_text = " ".join(clean_text.split())
    
    return clean_text

def scraping(documentUrl, path):

    driver = webdriver.Chrome()
    print(documentUrl)
    driver.get(documentUrl)
    try:
        wait = WebDriverWait(driver, 20)
        time.sleep(5)#adding implicit wait for larger documents
        body_present = EC.presence_of_element_located((By.TAG_NAME, 'body'))
        wait.until(body_present)
    except:
        print('unreachable')
        driver.quit()
        return

    html_source_code = driver.page_source
    html_soup = BeautifulSoup(html_source_code, 'html.parser')

    for s in html_soup.select('script'):
        s.extract()

    for i in html_soup.findAll("ix:header"):
        i.extract()
    
    documentUrl = documentUrl.split("/")
    documentUrl = documentUrl[-1]
    documentUrl = documentUrl.replace(".htm",'')
    if not ".txt" in documentUrl:
        completeName = os.path.join(path, documentUrl + ".txt")
    else:
        completeName =  os.path.join(path, documentUrl)

    f = open(completeName, "a+", encoding="utf-8")
    print(completeName)
    f.write(clean_htmll(html_soup.prettify()))
    f.close()
    driver.quit()
    print("File extracted.")

def extract_all_sublinks(url):
    try:
        # Replace 'chromedriver_path' with the actual path to your ChromeDriver executable
        driver = webdriver.Chrome()
        
        driver.get(url)
        
        sublinks = []

        # Find all the anchor elements
        anchor_elements = driver.find_elements(By.TAG_NAME, "a")

        for anchor in anchor_elements:
            sublink = anchor.get_attribute('href')
            if('/data/' in sublink):
                if sublink and (sublink.endswith('.htm') or sublink.endswith('.txt')):
                    sublinks.append(sublink)
        driver.quit()
        return sublinks

    except Exception as e:
        print(f"Error occurred: {e}")
        return []

def extract_all_text(url):
    try:
        driver = webdriver.Chrome()
        
        driver.get(url)
        all_text = driver.find_element_by_tag_name('body').text
        
        driver.quit()

        return all_text

    except Exception as e:
        print(f"Error occurred: {e}")
        return ""

'''
all_text = extract_all_text('https://www.sec.gov/ix?doc=/Archives/edgar/data/1326801/000132680121000014/fb-20201231.htm')
print(sublinks_list)
print(all_text)
'''