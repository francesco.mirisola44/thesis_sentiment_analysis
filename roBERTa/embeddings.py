import sys
import torch
from transformers import RobertaTokenizer, RobertaModel
import time
import os

def readEmbeddings(parent_dir, filename):
    path = os.path.join(parent_dir, filename)
    words = []
    f = open(path, "r", encoding="utf-8")

    for x in f:
        x = x.rstrip()
        words.append(x.lower())
    f.close()
    return words

def read_file(file):
    f = open(file, "r", encoding="utf-8")
    corpusText = f.read()
    return corpusText

def divide_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

def train_word_representations(list_sentences):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    print(device)

    tokenizer = RobertaTokenizer.from_pretrained('roberta-base')
    model = RobertaModel.from_pretrained('roberta-base')

    model.to(device)
    model.train()

    lr = 1e-2
    optimizer = torch.optim.AdamW(model.parameters(), lr=lr)

    max_len = 64
    epochs = 10

    epoch_all = []
    for epoch in range(epochs):
        epoch_losses = []
        epochs_losses_mean = 0
        counter = 0
        for sentence in list_sentences:
            input_ids = tokenizer.encode(sentence, add_special_tokens=True, padding='max_length', max_length=max_len, truncation=True, return_tensors='pt').to(device)
            
            outputs = model(input_ids)
            last_hidden_states = outputs.last_hidden_state

            # Calculate a custom loss if needed, based on your objective
            loss = torch.mean(last_hidden_states)  # Example: mean of hidden states

            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
            epoch_losses.append((epoch, loss.item()))
            epochs_losses_mean += loss.item()
            counter += 1
        epochs_losses_mean = epochs_losses_mean / counter
        print(f"Mean Epoch {epoch+1} Loss: {epochs_losses_mean}")
        epoch_all.append((epoch+1, epochs_losses_mean))
    
    # Save the updated model
    model.save_pretrained("./finetuned/Updated_Roberta/")
    return model, epoch_all

def training(list_sentences):
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    tokenizer = RobertaTokenizer.from_pretrained('roberta-base')

    model = RobertaModel.from_pretrained('roberta-base')

    model.to(device)
    model.train()

    lr = 1e-2

    optimizer = torch.optim.AdamW(model.parameters(), lr=lr)
    max_len = 64
    epochs = 10
    #max_grad_norm = 1.0
    epoch_all = []
    for epoch in range(epochs):
        for j in list_sentences:
            encoded_dict = tokenizer.batch_encode_plus(
                                j[1],
                                add_special_tokens = True,
                                max_length = max_len,
                                padding=True,
                                truncation=True,
                                return_attention_mask = True,
                                return_tensors = 'pt',
                        )
            optimizer.step()
            optimizer.zero_grad()

    model.save_pretrained("./finetuned/Fine_Tuned_Roberta")
    return model, epoch_all

def main() -> int:
    list_sentences = ['This one sentence', 'This is a second sentence']
    #x = list(divide_chunks(list_sentences, 20))
    x = list_sentences
    start_time = time.time()
    _, epoch_losses = training(x)
    end_time = time.time()
    training_time = end_time - start_time
    print("Training time:", time.strftime("%H:%M:%S", time.gmtime(training_time)))
    print(epoch_losses)
    return 0

if __name__ == '__main__':
    sys.exit(main())
