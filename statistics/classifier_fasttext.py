from gensim.models.fasttext import load_facebook_model
from sklearn.discriminant_analysis import StandardScaler
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
import joblib
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import GridSearchCV
from labelled import *
import numpy as np
from sklearn.utils import shuffle
import time
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sns
from nltk.tokenize import word_tokenize
import string
from nltk.corpus import words
from nltk.stem import PorterStemmer
from sklearn.metrics import confusion_matrix
import sys

from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import roc_auc_score, RocCurveDisplay
from sklearn.preprocessing import LabelBinarizer
from sklearn.decomposition import PCA
from sklearn.metrics import log_loss
import matplotlib.pyplot as plt
from sklearn.utils import check_matplotlib_support
check_matplotlib_support("agg")
from sklearn.metrics import auc, roc_curve
from sklearn.tree import DecisionTreeClassifier
from itertools import cycle

def create_confusion_matrix(true_labels, predicted_labels, num_classes):
    cm = confusion_matrix(true_labels, predicted_labels, labels=range(num_classes))
    return cm

def stemming(words):
    stemmer = PorterStemmer()
    stemmedWords = [stemmer.stem(word) for word in  word_tokenize(words)]
    return stemmedWords

def balance(input_df, desired_samples_per_label):
    label_0_df = input_df[input_df['labels'] == '0']
    label_1_df = input_df[input_df['labels'] == '1']
    label_2_df = input_df[input_df['labels'] == '2']

    sampled_label_0_df = label_0_df.sample(desired_samples_per_label, replace=True)
    sampled_label_1_df = label_1_df.sample(desired_samples_per_label, replace=True)
    sampled_label_2_df = label_2_df.sample(desired_samples_per_label, replace=True)

    balanced_df = pd.concat([sampled_label_0_df, sampled_label_1_df, sampled_label_2_df])

    return balanced_df

def build_embeddings_feature(model, text_list, embedding_function):
    embeddings = []
    for text in text_list:
        embedding = embedding_function(model, text)
        embeddings.append(embedding)
    mean_embeddings = np.mean(embeddings, axis=0)

    return mean_embeddings

def get_sentence_vector_stemmed(model, sentence):
    words = sentence.split()

    stemmed_words = [stemming(word)[0] for word in words]
    
    embeddings = [model.wv[word] for word in stemmed_words]

    if not embeddings:
        return np.zeros(model.vector_size)

    mean_embedding = np.mean(embeddings, axis=0)
    return mean_embedding

def get_sentence_vector(model, sentence):
    words = sentence.split()

    embeddings = [model.wv[word] for word in words]
    mean_embedding = np.mean(embeddings, axis=0)
    return mean_embedding

def remove_punctuation(tokens):
    punctuation = set(string.punctuation)
    filtered_tokens = [token for token in tokens if token not in punctuation]

    return filtered_tokens

def tokenize(text):
    english_words = set(words.words())
    tokens = word_tokenize(text)
    english_tokens = [token for token in tokens if token in english_words]
    return remove_punctuation(english_tokens)

def plot_embedded_scatter(embeddings, labels):
    pca = PCA(n_components=2)
    reduced_embeddings = pca.fit_transform(embeddings)
    
    unique_labels = np.unique(labels)
    colors = ['b', 'g', 'r']
    
    plt.figure()
    for label, color in zip(unique_labels, colors):
        class_embeddings = reduced_embeddings[labels == label]
        plt.scatter(class_embeddings[:, 0], class_embeddings[:, 1], color=color, label=f'Class {label}', s=5)
    
    plt.xlabel('Principal Component 1')
    plt.ylabel('Principal Component 2')
    plt.title('Multiclass Scatter Plot')
    legend_labels = {0: 'Negative', 1: 'Neutral', 2: 'Positive'}
    handles = [plt.Line2D([0], [0], marker='o', color='w', markerfacecolor=color, label=legend_labels[int(label)]) for label, color in zip(unique_labels, colors)]
    plt.legend(handles=handles)
    plt.savefig("scatter_plot_multiclass.png")

def roc_curve_graph(y_test, y_score):
    n_classes = 3
    '''
    micro_roc_auc_ovr = roc_auc_score(
        y_test,
        y_score,
        multi_class="ovr",
        average="micro",
    )

    print(f"Micro-averaged One-vs-Rest ROC AUC score:\n{micro_roc_auc_ovr:.2f}")
    # store the fpr, tpr, and roc_auc for all averaging strategies
    fpr, tpr, roc_auc = dict(), dict(), dict()
    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    print(f"Micro-averaged One-vs-Rest ROC AUC score:\n{roc_auc['micro']:.2f}")

    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    fpr_grid = np.linspace(0.0, 1.0, 1000)

    # Interpolate all ROC curves at these points
    mean_tpr = np.zeros_like(fpr_grid)

    for i in range(n_classes):
        mean_tpr += np.interp(fpr_grid, fpr[i], tpr[i])  # linear interpolation

    # Average it and compute AUC
    mean_tpr /= n_classes

    fpr["macro"] = fpr_grid
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    print(f"Macro-averaged One-vs-Rest ROC AUC score:\n{roc_auc['macro']:.2f}")

    plt.plot(
        fpr["micro"],
        tpr["micro"],
        label=f"micro-average ROC curve (AUC = {roc_auc['micro']:.2f})",
        color="deeppink",
        linestyle=":",
        linewidth=4,
    )

    plt.plot(
        fpr["macro"],
        tpr["macro"],
        label=f"macro-average ROC curve (AUC = {roc_auc['macro']:.2f})",
        color="navy",
        linestyle=":",
        linewidth=4,
    )
    '''

    fig, ax = plt.subplots(figsize=(6, 6))
    target_names = ['Negative', 'Neutral', 'Positive']
    colors = cycle(["aqua", "darkorange", "cornflowerblue"])
    for class_id, color in zip(range(n_classes), colors):
        RocCurveDisplay.from_predictions(
            y_test[:, class_id],
            y_score[:, class_id],
            name=f"ROC curve for {target_names[class_id]}",
            color=color,
            ax=ax,
            plot_chance_level=(class_id == 2),
        )

    plt.axis("square")
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("Extension of Receiver Operating Characteristic\nto One-vs-Rest multiclass")
    plt.legend()
    plt.savefig("aur_roc_multiclass_all.png")

def training_classifier(text_list, labels_list, pathSavedEmbeddings, pathSavingClassifier):
    print("Loading embeddings")
    model = load_facebook_model(pathSavedEmbeddings)

    #processed_data = [(get_sentence_vector_stemmed(model, ' '.join(tokenize(sentence))), label)
    #                for sentence, label in zip(text_list, labels_list) if tokenize(sentence)]
    #embeddings, labels_list = zip(*processed_data)

    embeddings = [get_sentence_vector(model, sentence) for sentence in text_list]
 
    X_train, X_test, y_train, y_test = train_test_split (embeddings, labels_list, test_size=0.2)
    
    pipe = make_pipeline(StandardScaler(), RandomForestClassifier())

    Pipeline(steps=[('standardscaler', StandardScaler()),
                    ('randomforestclassifier', RandomForestClassifier())])
    '''
    param_grid = {
        'randomforestclassifier__n_estimators': [50, 100, 200, 250, 300],
        'randomforestclassifier__max_depth': ['None', 5, 10, 20],
        'randomforestclassifier__max_features': ['auto', 'sqrt'],
        'randomforestclassifier__min_samples_leaf': [1, 2, 4],
        'randomforestclassifier__min_samples_split': [2, 5, 10],
        'randomforestclassifier__verbose': [1],
        'randomforestclassifier__n_jobs': [-1],
    }
    '''
    
    pipe = make_pipeline(StandardScaler(), LogisticRegression())
    Pipeline(steps=[('standardscaler', StandardScaler()),
                    ('logisticregression', LogisticRegression())])
    param_grid = {
        'logisticregression__C': [0.001, 0.01, 0.1, 1, 10],
        'logisticregression__solver': ['liblinear', 'lbfgs'],
        'logisticregression__max_iter': [100, 500, 1000, 10000],
        'logisticregression__class_weight': [None, 'balanced'],
        'logisticregression__multi_class': ['multinomial'],
        'logisticregression__verbose': [1],
        'logisticregression__n_jobs': [-1],
    }
    
    '''
    pipe = make_pipeline(StandardScaler(), DecisionTreeClassifier())
    Pipeline(steps=[('standardscaler', StandardScaler()),
                    ('decisiontreeclassifier', DecisionTreeClassifier())])
    param_grid = {
        'decisiontreeclassifier__max_depth': [None, 5, 10, 20, 50, 100],
        'decisiontreeclassifier__min_samples_split': [2, 5, 10],
        'decisiontreeclassifier__min_samples_leaf': [1, 2, 4],
        'decisiontreeclassifier__max_features': ['auto', 'sqrt', 'log2'],
    }
    '''
    grid = GridSearchCV(pipe, param_grid, cv=5, n_jobs=-1)
    grid.fit(X_train, y_train)

    best_model = grid.best_estimator_
    print('Best estimator: ', best_model)

    joblib.dump(best_model, pathSavingClassifier)

    insample_predictions = best_model.predict(X_train)

    print("Insample Accuracy {0:.2f}%".format(100*accuracy_score(insample_predictions,y_train)))
    print(classification_report(insample_predictions,y_train))

    outsample_predictions = best_model.predict(X_test)
    print("Accuracy {0:.2f}%".format(100*accuracy_score(outsample_predictions, y_test)))
    print(classification_report(y_test, outsample_predictions))

def make_predictions(text_list, pathSavedEmbeddings, pathSavedClassifier):
    loaded_model = joblib.load(pathSavedClassifier)
    model = load_facebook_model(pathSavedEmbeddings)

    predicted_labels = []
    predicted_probabilities = []

    embeddings = [get_sentence_vector(model, sentence) for sentence in text_list] #unclean

    #embeddings = [get_sentence_vector_stemmed(model, ' '.join(tokenize(sentence))) for sentence in text_list] # clean

    predicted_labels = loaded_model.predict(embeddings)
    predicted_probabilities = loaded_model.predict_proba(embeddings)

    df = pd.DataFrame(columns=["Text", "Predicted Label", "Negative", "Neutral", "Positive"])
    for text, label, probs in zip(text_list, predicted_labels, predicted_probabilities):
        df = pd.concat([df, pd.DataFrame({"Text": [text], "Predicted Label": [label], "Negative": [probs[0]], "Neutral": [probs[1]], "Positive": [probs[2]]})], ignore_index=True)
    plot_embedded_scatter(embeddings, predicted_labels)
    return df

def avg_prob(df):
    avg_0 = 0
    avg_1 = 0
    avg_2 = 0

    count_0 = df[(df['labels'] == df['Predicted Label']) & (df['labels'] == 0)].shape[0]
    count_1 = df[(df['labels'] == df['Predicted Label']) & (df['labels'] == 1)].shape[0]
    count_2 = df[(df['labels'] == df['Predicted Label']) & (df['labels'] == 2)].shape[0]

    for _, i in df.iterrows():
        if i['labels'] == i['Predicted Label'] and i['labels'] == 0:
            avg_0 += i['Negative']
        if i['labels'] == i['Predicted Label'] and i['labels'] == 1:
            avg_1 += i['Neutral']
        if i['labels'] == i['Predicted Label'] and i['labels'] == 2:
            avg_2 += i['Positive']

    avg_0 = avg_0 / count_0 if count_0 != 0 else 0
    avg_1 = avg_1 / count_1 if count_1 != 0 else 0
    avg_2 = avg_2 / count_2 if count_2 != 0 else 0
    return avg_0, avg_1, avg_2

def read_general(path):
    df = read_csv(path)
    df = df.dropna(subset=['sentiment'])
    df = df.dropna(subset=['text'])
    print(len(df))
    df['labels'] = df['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    df = balance(df, 1500)
    return df

def read_financial(path):
    df = read_parquet(path)
    df = shuffle(df)
    df = df[:4500]
    return df

def main() -> int:
    path_classifier = './FastText/classifiers/classifier_logistic_financial_unclean'
    
    '''
    text_list = df['text'].astype(str).str.lower().tolist()
    labels_list = df['labels'].tolist()
    '''

    df = read_financial('./sentiment_db/financial/train-00000-of-00001.parquet')
    
    #df = read_general('./sentiment_db/general/train.csv')
 
    text_list = df['text'].astype(str).str.lower().tolist()
    labels_list = df['labels'].tolist()
    start_time = time.time()

    print("training...")
    training_classifier(text_list, labels_list, './FastText/embeddings/cc.en.300.bin', path_classifier)

    end_time = time.time()
    training_time = end_time - start_time

    print("Training Time:", training_time, "seconds")


    df_parquet = read_parquet('./sentiment_db/financial/test-00000-of-00001.parquet')
    text_list = df_parquet['text'].astype(str).str.lower().tolist()

    
    #df_csv = read_csv('./sentiment_db/general/test.csv')
    #text_list = df_csv['text'].astype(str).str.lower().tolist()

    start_time = time.time()
    df = make_predictions(text_list, './FastText/embeddings/cc.en.300.bin', path_classifier)
    end_time = time.time()
    prediction_time = end_time - start_time

    print("Predictions Time:", prediction_time, "seconds")
    
    #df['labels'] = df_csv['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'}) #for General dataset
    
    df['labels'] = df_parquet['labels'] # for Financial dataset

    df.to_excel("test.xlsx", index=False)

    df = pd.read_excel('test.xlsx')
    print(df.head())

    score = roc_auc_score(df['labels'].tolist(), df[['Negative', 'Neutral', 'Positive']].astype(float).values.tolist(), multi_class='ovr')

    print(f"Multiclass ROC AUC score: {score:.2f}")
    label_binarizer = LabelBinarizer().fit(df['labels'].tolist())
    y_test = label_binarizer.transform(df['labels'].tolist())
    print(y_test)
    roc_curve_graph(y_test, df[['Negative', 'Neutral', 'Positive']].astype(float).values)

    true_labels = df['labels']
    predicted_labels = df['Predicted Label']
    num_classes = 3

    confusion_matrix_result = create_confusion_matrix(true_labels, predicted_labels, num_classes)

    print("Confusion Matrix:")
    print(confusion_matrix_result)

    class_labels = ["Negative", "Neutral", "Positive"]

    plt.figure(figsize=(8, 6))
    sns.set(font_scale=1.2)
    sns.heatmap(confusion_matrix_result, annot=True, fmt="d", cmap="Blues",
                xticklabels=class_labels,
                yticklabels=class_labels)
    
    plt.xlabel("Predicted")
    plt.ylabel("True")
    plt.title("Confusion Matrix")

    plt.savefig('confusion_matrix.png')

    accuracy_per_label = np.diag(confusion_matrix_result) / np.sum(confusion_matrix_result, axis=1)

    for i in range(num_classes):
        print(f"Accuracy for Class {i}: {accuracy_per_label[i]:.2f}")

    print(accuracy_score(true_labels, predicted_labels))

    classes = ['Negative', 'Neutral', 'Positive']
    average_probabilities = [*avg_prob(df)]

    plt.figure(figsize=(8, 6))
    sns.set(style="whitegrid")
    sns.barplot(x=classes, y=average_probabilities)
    plt.xlabel("Class")
    plt.ylabel("Average Predicted Probability")
    plt.title("Average Predicted Probabilities for Correctly Labeled Classes")

    plt.savefig('confidence.png')

    return 0

if __name__ == '__main__':
    sys.exit(main()) 