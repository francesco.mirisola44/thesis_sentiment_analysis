import os

def read_embeddings(parent_dir, filename):
    path = os.path.join(parent_dir, filename)
    words = []
    f = open(path, "r", encoding="utf-8")

    for x in f:
        x = x.rstrip()
        words.append(x.lower())
    f.close()
    return words