

from stats import *
from prediction_gpt_2 import *
from classifier_fasttext import get_sentence_vector
from gensim.models.fasttext import load_facebook_model
from classifier_roberta import load_bert_classifier, predict
from collections import defaultdict
import joblib
import heapq

def make_predictions(text_list, loaded_model, model):
    embeddings = [get_sentence_vector(model, sentence) for sentence in text_list]

    predicted_labels = loaded_model.predict(embeddings)
    predicted_probabilities = loaded_model.predict_proba(embeddings)
    df = pd.DataFrame(columns=["Text", "Predicted Label", "Negative", "Neutral", "Positive"])
    for text, label, probs in zip(text_list, predicted_labels, predicted_probabilities):
        df = pd.concat([df, pd.DataFrame({"Text": [text], "Predicted Label": [label], "Negative": [probs[0]], "Neutral": [probs[1]], "Positive": [probs[2]]})], ignore_index=True)
    return df

root_directory = r'.\companies'
#pathSavedEmbeddings = './FastText/embeddings/cc.en.300.bin'
#pathSavedClassifier = './FastText/classifiers/classifier_logistic_financial_unclean'

'''
esg_terms = read_embeddings('./ESG', 'Social.txt')
list_esg = get_occurences_esg_per_year_per_company(root_directory, esg_terms)

environmental_terms = read_embeddings('./ESG', 'Environmental.txt')
list_environmental = get_occurences_esg_per_year_per_company(root_directory, environmental_terms)

governance_terms = read_embeddings('./ESG', 'Governance.txt')
list_governance = get_occurences_esg_per_year_per_company(root_directory, governance_terms)
'''
esg_terms = read_embeddings('./ESG', 'esg.txt')
list_esg = get_occurences_esg_per_year_per_company(root_directory, esg_terms)

label_counts_per_year = {}
for company_name, year_data in list_esg.items():
    for year, sentences in year_data.items():
        label_counts_per_year[year] = {'Negative': 0, 'Neutral': 0, 'Positive': 0}
label_counts_per_year = dict(sorted(label_counts_per_year.items(), key=lambda x: int(x[0])))

#loaded_model = joblib.load(pathSavedClassifier) #fasttext
#model = load_facebook_model(pathSavedEmbeddings) #fasttext

#model = load_gpt2() #gpt2
#tokenizer = load_tokenizer() #gpt2

#model_path = './robertamodel/model_final_roberta_general_uncleaned.pt' #roberta

model_path = './roBERTa/transfer/model_final_4500_dp0.2_batch4_lr0.01_epoch5.pt'

print("loading")
model, tokenizer = load_bert_classifier(model_path) #roberta
print("Loaded roberta")

most_positive_esg_terms = defaultdict(lambda: defaultdict(str))
most_negative_esg_terms = defaultdict(lambda: defaultdict(str))
most_neutral_esg_terms = defaultdict(lambda: defaultdict(str))

company_sentiment_counts = defaultdict(lambda: {'Positive': 0, 'Negative': 0, 'Neutral': 0})
year_company_sentiment_counts = defaultdict(lambda: defaultdict(lambda: {'Positive': 0, 'Negative': 0, 'Neutral': 0}))
company_sentences_by_sentiment = defaultdict(lambda: defaultdict(list))

def add_sentence(sentences_list, sentence):
    sentences_list.append(sentence)
    if len(sentences_list) > 1:
        heapq.heappop(sentences_list)

for company_name, year_data in list_esg.items():
    for year, sentences in year_data.items():
        if len(sentences) > 0:
            positive_terms_counter = Counter()
            negative_terms_counter = Counter()
            neutral_terms_counter = Counter()
            for sentence in sentences:
                #df = make_predictions([sentence], loaded_model, model) #fasttext
                #predicted_label = df['Predicted Label'].astype(str).tolist() #fasttext
                
                #predicted_label = list(predict_sentiment(get_probabilities(sentence, model, tokenizer))) #gpt2
                predicted_label, _, _, _ = predict(model, tokenizer, sentence)
                
                if (predicted_label[0] == '1'):
                    company_sentiment_counts[company_name]['Neutral'] += 1
                    year_company_sentiment_counts[company_name][year]['Neutral'] += 1
                    label_counts_per_year[year]['Neutral'] += 1
                    add_sentence(company_sentences_by_sentiment[company_name]['Neutral'], sentence)
                elif (predicted_label[0] == '2'):
                    company_sentiment_counts[company_name]['Positive'] += 1
                    label_counts_per_year[year]['Positive'] += 1
                    year_company_sentiment_counts[company_name][year]['Positive'] += 1
                    add_sentence(company_sentences_by_sentiment[company_name]['Positive'], sentence)
                elif (predicted_label[0] == '0'):
                    company_sentiment_counts[company_name]['Negative'] += 1
                    label_counts_per_year[year]['Negative'] += 1
                    year_company_sentiment_counts[company_name][year]['Negative'] += 1
                    add_sentence(company_sentences_by_sentiment[company_name]['Negative'], sentence)
                
                terms = sentence.split()
                
                for term in esg_terms:
                    if term in terms:
                        if predicted_label[0] == '1':
                            neutral_terms_counter[term] += 1
                        elif predicted_label[0] == '2':
                            positive_terms_counter[term] += 1
                        elif predicted_label[0] == '0':
                            negative_terms_counter[term] += 1

    years = list(label_counts_per_year.keys())
    positive_counts = [counts['Positive'] for counts in label_counts_per_year.values()]
    negative_counts = [counts['Negative'] for counts in label_counts_per_year.values()]
    neutral_counts = [counts['Neutral'] for counts in label_counts_per_year.values()]

    dominant_sentiments = []
    for counts in label_counts_per_year.values():
        max_sentiment = max(counts, key=counts.get)
        dominant_sentiments.append(max_sentiment)

    sentiment_values = {
        'Positive': 1,
        'Neutral': 0,
        'Negative': -1
    }

    curve_points = [sentiment_values[sentiment] for sentiment in dominant_sentiments]
    sentiment_labels = ['Negative', 'Neutral', 'Positive']

    plt.figure(figsize=(10, 6))
    plt.plot(years, curve_points, marker='o', linestyle='-', color='b', label='Sentiment Curve')
    plt.xlabel('Year')
    plt.ylabel('Sentiment Level')
    plt.xticks(rotation=45)
    plt.title(f'Sentiment Trend Over Years for {company_name}')
    plt.yticks(list(sentiment_values.values()), sentiment_labels) 
    plt.legend()
    plt.grid(True)
    plt.savefig(f'{company_name}_sentiment_curve.png')

most_common_sentiment_by_company = {}
for company_name, sentiment_counts in company_sentiment_counts.items():
    most_common_sentiment = max(sentiment_counts, key=sentiment_counts.get)
    most_common_sentiment_by_company[company_name] = most_common_sentiment

print("Most common sentiment by company:", most_common_sentiment_by_company)

year_per_company_most_positive = {}
year_per_company_most_negative = {}
year_per_company_most_neutral = {}

for company_name, year_counts in year_company_sentiment_counts.items():
    max_positive_year = max(year_counts, key=lambda year: year_counts[year]['Positive'], default=None)
    max_negative_year = max(year_counts, key=lambda year: year_counts[year]['Negative'], default=None)
    max_neutral_year = max(year_counts, key=lambda year: year_counts[year]['Neutral'], default=None)

    if max_positive_year is not None:
        year_per_company_most_positive[company_name] = max_positive_year
    if max_negative_year is not None:
        year_per_company_most_negative[company_name] = max_negative_year
    if max_neutral_year is not None:
        year_per_company_most_neutral[company_name] = max_neutral_year

print("Year per company with most positive sentiment:", year_per_company_most_positive)
print("Year per company with most negative sentiment:", year_per_company_most_negative)
print("Year per company with most neutral sentiment:", year_per_company_most_neutral)

most_common_sentiment_by_company = {}
for company_name, sentiment_counts in company_sentiment_counts.items():
    most_common_sentiment = max(sentiment_counts, key=sentiment_counts.get)
    most_common_sentiment_by_company[company_name] = most_common_sentiment

print("Most common sentiment by company:", most_common_sentiment_by_company)

plt.figure(figsize=(10, 6))
plt.plot(years, positive_counts, label='Positive')
plt.plot(years, negative_counts, label='Negative')
plt.plot(years, neutral_counts, label='Neutral')
plt.xlabel('Year')
plt.ylabel('Label Count')
plt.xticks(rotation=45)
plt.title('Evolution of Predicted Labels Over Years for all companies')
plt.legend()
plt.grid(True)
plt.savefig('all_sentiment_esg.png')

for company_name, sentiment_data in company_sentences_by_sentiment.items():
    for sentiment, sentences_list in sentiment_data.items():
        if(sentiment == 'Neutral'):
            print(f"Top 1 {sentiment} sentences for {company_name}:")
            for idx, sentence in enumerate(reversed(sentences_list)):
                print(f"{idx + 1}: {sentence}")