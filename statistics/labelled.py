
import pandas as pd

def read_parquet(file):
    return pd.read_parquet(file)

def read_csv(file):
    return pd.read_csv(file)

def get_counts(df):
    return df['labels'].value_counts()[1], df['labels'].value_counts()[0], df['labels'].value_counts()[2]