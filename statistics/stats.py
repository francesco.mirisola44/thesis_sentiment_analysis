import os
import sys
import nltk
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
import re

nltk.download('punkt')
nltk.download('words')

stop_words = set(stopwords.words('english'))

from nltk.corpus import words
from nltk.tokenize import word_tokenize, sent_tokenize
import matplotlib.pyplot as plt
import string
from collections import Counter
from esg import read_embeddings
from labelled import *

def remove_punctuation(tokens):
    # List of punctuation characters
    punctuation = set(string.punctuation)

    # Filter out tokens that consist only of punctuation characters
    filtered_tokens = [token for token in tokens if token not in punctuation]

    return filtered_tokens

def stemming(words):
    stemmer = PorterStemmer()
    stemmedWords = [stemmer.stem(word) for word in  word_tokenize(words)]
    return stemmedWords

def remove_stopwords(text):
    words = word_tokenize(text)
    filtered_words = [word for word in words if word.lower() not in stop_words]
    filtered_text = ' '.join(filtered_words)
    return filtered_text.lower()

def count_words(input_string):
    words = word_tokenize(input_string)
    return len(words)

def count_sentences(input_string):
    sentences = sent_tokenize(input_string)
    return len(sentences)

def get_years_subfolders(company_name, industry_type, root_directory):
    company_folder_path = os.path.join(root_directory, industry_type, company_name)

    if not os.path.exists(company_folder_path):
        raise ValueError(f"Company '{company_name}' not found in the specified industry folder.")

    years_subfolders = []
    for year_folder in os.listdir(company_folder_path):
        year_folder_path = os.path.join(company_folder_path, year_folder)
        if os.path.isdir(year_folder_path):
            years_subfolders.append(year_folder)

    return years_subfolders

def read_text_files_in_year_subfolder(year_subfolder_path):
    if not os.path.exists(year_subfolder_path):
        raise ValueError(f"The year subfolder path '{year_subfolder_path}' does not exist.")

    file_contents = []
    for file_name in os.listdir(year_subfolder_path):
        file_path = os.path.join(year_subfolder_path, file_name)
        if os.path.isfile(file_path) and file_name.endswith('.txt'):
            with open(file_path, 'r', encoding='utf-8') as file:
                file_contents.append(file.read())

    return '\n'.join(file_contents)

def count_words_in_text_files(year_subfolder_path):
    if not os.path.exists(year_subfolder_path):
        raise ValueError(f"The year subfolder path '{year_subfolder_path}' does not exist.")

    word_counts = []
    for file_name in os.listdir(year_subfolder_path):
        file_path = os.path.join(year_subfolder_path, file_name)
        if os.path.isfile(file_path) and file_name.endswith('.txt'):
            with open(file_path, 'r', encoding='utf-8') as file:
                content = file.read()
                words = content.split()
                word_counts.append(len(words))

    return word_counts

def get_english_vocabulary(top_n=10):
    english_vocabulary = set(words.words())
    word_counts = {word: words.words().count(word) for word in english_vocabulary}
    most_common_words = sorted(word_counts.items(), key=lambda x: x[1], reverse=True)[:top_n]
    return most_common_words

def get_folders(directory):
    folders = []
    for item in os.listdir(directory):
        item_path = os.path.join(directory, item)
        if os.path.isdir(item_path):
            folders.append(item_path)
            subfolders = get_folders(item_path)
            folders.extend(subfolders)
    return folders

def get_unique_english_words(file_paths):
    english_words = set(words.words())
    unique_english_words = set()

    for file_path in file_paths:
        if os.path.isfile(file_path):
            with open(file_path, 'r', encoding='utf-8') as file:
                text = file.read().lower()
                tokens = word_tokenize(text)
                english_tokens = [token for token in tokens if token in english_words]
                unique_english_words.update(english_tokens)

    return remove_punctuation(unique_english_words)


def get_unique_stemmed_words(file_paths):
    english_words = set(words.words())
    unique_stemmed_words = set()
    
    porter = PorterStemmer()

    for file_path in file_paths:
        if os.path.isfile(file_path):
            with open(file_path, 'r', encoding='utf-8') as file:
                text = file.read().lower()
                tokens = word_tokenize(text)
                stemmed_tokens = [porter.stem(token) for token in tokens if token in english_words]
                unique_stemmed_words.update(stemmed_tokens)
    return remove_punctuation(unique_stemmed_words)

def get_full_path(year_subfolder_path):
    if not os.path.exists(year_subfolder_path):
        raise ValueError(f"The year subfolder path '{year_subfolder_path}' does not exist.")

    file_paths = []
    for file_name in os.listdir(year_subfolder_path):
        file_path = os.path.join(year_subfolder_path, file_name)
        if os.path.isfile(file_path) and file_name.endswith('.txt'):
            file_paths.append(file_path)

    return file_paths

def calculate_total_tokens(year_occurrences_set, ls_years, ls_numbers):
    for year, tokens in zip(ls_years, ls_numbers):
        if year in year_occurrences_set:
            year_occurrences_set[year] += tokens
        else:
            year_occurrences_set[year] = tokens

def tokenize_and_filter(text):
    english_words = set(words.words())
    tokens = word_tokenize(text)
    english_tokens = [token.lower() for token in tokens if token.lower() in english_words]
    return len(remove_punctuation(english_tokens))

def tokenize(text):
    english_words = set(words.words())
    tokens = word_tokenize(text)
    english_tokens = [token.lower() for token in tokens if token.lower() in english_words]
    return remove_punctuation(english_tokens)

def tokenize_uncleaned(text):
    tokens = word_tokenize(text)
    return len(tokens)

def stem_tokens(text):
    english_words = set(words.words())
    tokens = word_tokenize(text)
    
    porter = PorterStemmer()
    stemmed_tokens = [porter.stem(token.lower()) for token in tokens if token.lower() in english_words]
    print(stemmed_tokens)
    
    return len(stemmed_tokens)

def plot_tokens(company_name, nbr_words, years):
    plt.figure(figsize=(8, 6))
    plt.bar(years, nbr_words)
    plt.xlabel('Years')
    plt.ylabel('Number of tokens')
    plt.title(f'10-K files number of tokens per year for {company_name}')
    plt.xticks(rotation=45)

    filename = f'{company_name}.png'
    plt.savefig(filename)

def calculate_average_token_count(document_paths):
    total_tokens = 0
    total_documents = len(document_paths)

    for doc_path in document_paths:
        with open(doc_path, "r", encoding="utf-8") as file:
            content = file.read()
            tokens = word_tokenize(content)
            total_tokens += len(tokens)

    average_token_count = total_tokens / total_documents
    return average_token_count

def calculate_average_filtered_token_count(document_paths):
    english_stopwords = set(stopwords.words("english"))
    english_words = set(words.words())
    
    total_filtered_tokens = 0
    total_documents = len(document_paths)

    for doc_path in document_paths:
        with open(doc_path, "r", encoding="utf-8") as file:
            content = file.read()
            tokens = word_tokenize(content)
            filtered_tokens = [token for token in tokens if token.lower() not in english_stopwords and token.lower() in english_words]
            total_filtered_tokens += len(filtered_tokens)

    average_filtered_token_count = total_filtered_tokens / total_documents
    return average_filtered_token_count

def get_most_frequent_words(document_paths, num_words=20):
    punctuation = set(string.punctuation)
    english_stopwords = set(stopwords.words("english"))
    english_words = set(words.words())

    all_tokens = []

    for doc_path in document_paths:
        with open(doc_path, "r", encoding="utf-8") as file:
            content = file.read()
            tokens = word_tokenize(content)
            filtered_tokens = [token.lower() for token in tokens if token.lower() not in punctuation and token.lower() not in english_stopwords and token.lower() in english_words]
            all_tokens.extend(filtered_tokens)

    word_counter = Counter(all_tokens)
    most_common_words = word_counter.most_common(num_words)

    return most_common_words

def get_most_frequent_words_from_sentences(sentences, num_words=20):
    punctuation = set(string.punctuation)
    english_stopwords = set(stopwords.words("english"))
    english_words = set(words.words())

    all_tokens = []

    for sentence in sentences:
        tokens = word_tokenize(sentence)
        filtered_tokens = [token.lower() for token in tokens if token.lower() not in punctuation and token.lower() not in english_stopwords and token.lower() in english_words]
        all_tokens.extend(filtered_tokens)

    word_counter = Counter(all_tokens)
    most_common_words = word_counter.most_common(num_words)

    return most_common_words

def tfidf(document_paths):
    document_contents = []

    for doc_path in document_paths:
        with open(doc_path, "r", encoding="utf-8") as file:
            content = file.read()
            document_contents.append(content)

    english_words = set(words.words())

    vectorizer = TfidfVectorizer(stop_words="english", vocabulary=english_words)
    
    tfidf_matrix = vectorizer.fit_transform(document_contents)
    feature_names = vectorizer.get_feature_names_out()

    return tfidf_matrix, feature_names

def display_most_important_words(tfidf_matrix, feature_names, num_words=20):
    average_tfidf_scores = tfidf_matrix.mean(axis=0)
    
    average_tfidf_scores = average_tfidf_scores.flatten()
    
    sorted_indices = average_tfidf_scores.argsort()[::-1]
    
    top_feature_names = [feature_names[index] for index in sorted_indices[:num_words]]
    
    print("Most Important Words:")
    for word in top_feature_names:
        print(word[0])

def get_general_statistics(root_directory):
    all_folders = get_folders(root_directory)
    list_full_paths = []
    year_tokens_set = {}
    nbr_sentences = 0
    document_contents = []

    for folder in all_folders:
        if(len(folder.split('\\')) == 4):
            company_name = folder.split('\\')[-1]
            industry_type = folder.split('\\')[-2]
            
            ls_years = []
            ls_nbr_tokens = []
            years_subfolders = get_years_subfolders(company_name, industry_type, root_directory)
            for i in years_subfolders:
                fullpath = root_directory + '/' + industry_type + '/' + company_name  + '/' + i
                ls_years.append(i)
                ls_nbr_tokens.append(tokenize_and_filter(read_text_files_in_year_subfolder(fullpath)))
                
                list_full_paths += get_full_path(fullpath)
                calculate_total_tokens(year_tokens_set, ls_years, ls_nbr_tokens)
                nbr_sentences += count_sentences(read_text_files_in_year_subfolder(fullpath))

                document_contents.append(tokenize_and_filter(read_text_files_in_year_subfolder(fullpath)))

            plot_tokens(company_name, ls_nbr_tokens, ls_years)
    sorted_occurrences = dict(sorted(year_tokens_set.items(), key=lambda item: item[0]))

    plt.figure(figsize=(10, 6))
    plt.bar(sorted_occurrences.keys(), sorted_occurrences.values())
    plot_tokens('All Industries.png', sorted_occurrences.values(), sorted_occurrences.keys())
    print("Number of sentences", nbr_sentences)
    print("Number of unique english words", len(get_unique_english_words(list_full_paths)))
    print("Number unique english words stemmed", len(get_unique_stemmed_words(list_full_paths)))
    print("Total Word Occurrences across all years:", sum(sorted_occurrences.values()))
    print(len(list_full_paths))
    print('Average token number', calculate_average_token_count(list_full_paths))
    print('Average token number without stop words', calculate_average_filtered_token_count(list_full_paths))

    tfidf_array, feature_names =  tfidf(list_full_paths)
    print("TF-IDF")
    print(get_most_frequent_words(list_full_paths))
    tfidf_array, feature_names = tfidf(list_full_paths)
    display_most_important_words(tfidf_array, feature_names, 20)

def get_occurences_esg(root_directory, list_words):
    all_folders = get_folders(root_directory)
    word_counts = {}

    for folder in all_folders:
        if len(folder.split('\\')) == 4:
            company_name = folder.split('\\')[-1]
            industry_type = folder.split('\\')[-2]
            
            years_subfolders = get_years_subfolders(company_name, industry_type, root_directory)
            for year_subfolder in years_subfolders:
                fullpath = os.path.join(root_directory, industry_type, company_name, year_subfolder)  
                doc = read_text_files_in_year_subfolder(fullpath)
                doc = doc.lower()
                
                for word in list_words:
                    word_count = doc.count(word)
                    if word in word_counts:
                        word_counts[word] += word_count
                    else:
                        word_counts[word] = word_count

    for word, count in word_counts.items():
        print(f"'{word}' found {count} times in the text corpus.")

def get_occurences_esg_per_year_per_company(root_directory, list_words):
    all_folders = get_folders(root_directory)
    companies_data = {}

    for folder in all_folders:
        if len(folder.split('\\')) == 4:
            company_name = folder.split('\\')[-1]
            industry_type = folder.split('\\')[-2]
            
            years_subfolders = get_years_subfolders(company_name, industry_type, root_directory)
            
            if company_name not in companies_data:
                companies_data[company_name] = {}
            
            for year_subfolder in years_subfolders:
                fullpath = os.path.join(root_directory, industry_type, company_name, year_subfolder)  
                doc = read_text_files_in_year_subfolder(fullpath)
                doc = doc.lower()
                
                if year_subfolder not in companies_data[company_name]:
                    companies_data[company_name][year_subfolder] = set()
                
                sentences = sent_tokenize(doc)
                for sentence in sentences:
                    for word in list_words:
                        if re.search(r'\b' + re.escape(word) + r'\b', sentence):
                            companies_data[company_name][year_subfolder].add(sentence)

    return companies_data

def average_length_sentences(ls_sentences):
    avg = 0
    for i in ls_sentences:
        avg += tokenize_and_filter(i)
    return avg / len(ls_sentences)

def average_length_sentences_uncleaned(ls_sentences):
    avg = 0
    for i in ls_sentences:
        avg += tokenize_uncleaned(i)
    return avg / len(ls_sentences)

def count_word_occurrences(sentences, words):
    word_counts = {word: 0 for word in words}

    for sentence in sentences:
        for word in words:
            word_counts[word] += sentence.lower().split().count(word.lower())

    return word_counts

def main() -> int:
    root_directory = r'.\companies'
    #get_general_statistics(root_directory)
    '''
    environmental_terms = read_embeddings('./ESG', 'Environmental.txt')
    get_occurences_esg(root_directory, environmental_terms)
    governance_terms = read_embeddings('./ESG', 'Governance.txt')
    get_occurences_esg(root_directory, governance_terms)

    social_terms = read_embeddings('./ESG', 'Social.txt')
    get_occurences_esg(root_directory, social_terms)

    environmental_terms = read_embeddings('./ESG', 'Environmental.txt')
    get_occurences_esg(root_directory, environmental_terms)
    governance_terms = read_embeddings('./ESG', 'Governance.txt')
    get_occurences_esg(root_directory, governance_terms)
    '''

    #social_terms = read_embeddings('./ESG', 'Social.txt')
    #get_occurences_esg_per_year_per_company(root_directory, social_terms, 'Apple Inc')
    #get_occurences_esg(root_directory, social_terms)
    '''
    df_parquet = read_parquet('./sentiment_db/financial/train-00000-of-00001.parquet')
    print(get_counts(df_parquet))
    df_csv = read_csv('./sentiment_db/general/train.csv')
    df_csv['labels'] = df_csv['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    print(get_counts(df_csv))

    print(average_length_sentences(df_csv['text'].astype(str).tolist()))
    print(average_length_sentences(df_parquet['text'].astype(str).tolist()))

    df_parquet = read_parquet('./sentiment_db/financial/test-00000-of-00001.parquet')
    print(get_counts(df_parquet))
    df_csv = read_csv('./sentiment_db/general/test.csv')
    df_csv['labels'] = df_csv['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    print(get_counts(df_csv))

    print(average_length_sentences(df_csv['text'].astype(str).tolist()))
    print(average_length_sentences(df_parquet['text'].astype(str).tolist()))
    '''
    '''
    df_parquet = read_parquet('./sentiment_db/financial/train-00000-of-00001.parquet')
    
    df_csv = read_csv('./sentiment_db/general/train.csv')
    df_csv['labels'] = df_csv['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    social_terms = read_embeddings('./ESG', 'Social.txt')
    environmental_terms = read_embeddings('./ESG', 'Environmental.txt')
    governance_terms = read_embeddings('./ESG', 'Governance.txt')
    '''
    '''
    print(count_word_occurrences(df_csv['text'].astype(str).str.lower().tolist(), social_terms))
    print(count_word_occurrences(df_csv['text'].astype(str).str.lower().tolist(), environmental_terms))
    print(count_word_occurrences(df_csv['text'].astype(str).str.lower().tolist(), governance_terms))
    '''
    '''
    print(count_word_occurrences(df_parquet['text'].astype(str).str.lower().tolist(), social_terms))
    print(count_word_occurrences(df_parquet['text'].astype(str).str.lower().tolist(), environmental_terms))
    print(count_word_occurrences(df_parquet['text'].astype(str).str.lower().tolist(), governance_terms))

    print(get_most_frequent_words_from_sentences(df_parquet['text'].astype(str).str.lower().tolist(), 10))
    print(get_most_frequent_words_from_sentences(df_csv['text'].astype(str).str.lower().tolist(), 10))
    '''
    '''
    print(get_counts(df_csv))

    print(average_length_sentences_uncleaned(df_csv['text'].astype(str).tolist()))
    print(average_length_sentences_uncleaned(df_parquet['text'].astype(str).tolist()))

    df_parquet = read_parquet('./sentiment_db/financial/test-00000-of-00001.parquet')
    print(get_counts(df_parquet))
    df_csv = read_csv('./sentiment_db/general/test.csv')
    df_csv['labels'] = df_csv['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    print(get_counts(df_csv))

    print(average_length_sentences_uncleaned(df_csv['text'].astype(str).tolist()))
    print(average_length_sentences_uncleaned(df_parquet['text'].astype(str).tolist()))
    '''
    #social_terms = read_embeddings('./ESG', 'Social.txt')
    #get_occurences_esg_per_year_per_company(root_directory, social_terms)
    return 0

if __name__ == '__main__':
    sys.exit(main()) 