
import torch
from tqdm import tqdm
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import classification_report, accuracy_score
from transformers import (set_seed,
                          GPT2Config,
                          GPT2Tokenizer, 
                          get_linear_schedule_with_warmup,
                          GPT2ForSequenceClassification)
import pandas as pd
import matplotlib.pyplot as plt

def balance(input_df, desired_samples_per_label):
    label_0_df = input_df[input_df['labels'] == '0']
    label_1_df = input_df[input_df['labels'] == '1']
    label_2_df = input_df[input_df['labels'] == '2']

    sampled_label_0_df = label_0_df.sample(desired_samples_per_label, replace=True)
    sampled_label_1_df = label_1_df.sample(desired_samples_per_label, replace=True)
    sampled_label_2_df = label_2_df.sample(desired_samples_per_label, replace=True)

    balanced_df = pd.concat([sampled_label_0_df, sampled_label_1_df, sampled_label_2_df])

    return balanced_df
set_seed(112)

epochs = 10

batch_size = 4

max_length = 512

# Look for gpu to use. Will use `cpu` by default if no gpu found.
device = "cpu"#torch.device('cuda' if torch.cuda.is_available() else 'cpu')

model_name_or_path = 'gpt2'

labels_ids = {'0': 0, '1': 1, '2': 2}

n_labels = len(labels_ids)

def show_stats(all_acc, all_loss):
  # Plot loss
  plt.figure(figsize=(6, 4))
  plt.plot(all_loss['train_loss'], label='Train Loss')
  plt.plot(all_loss['val_loss'], label='Validation Loss')
  plt.xlabel('Epoch')
  plt.ylabel('Loss')
  plt.legend()
  plt.title('Loss Over Epochs')
  plt.tight_layout()
  plt.savefig('loss_plot.png')
  plt.show()

  # Plot accuracy
  plt.figure(figsize=(6, 4))
  plt.plot(all_acc['train_acc'], label='Train Accuracy')
  plt.plot(all_acc['val_acc'], label='Validation Accuracy')
  plt.xlabel('Epoch')
  plt.ylabel('Accuracy')
  plt.legend()
  plt.title('Accuracy Over Epochs')
  plt.tight_layout()
  plt.savefig('accuracy_plot.png')
  plt.show()

class ClassifierDataset(Dataset):

  def __init__(self, path):

    self.texts = []
    self.labels = []

    content = pd.read_csv(path)
    content['labels'] = content['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    content = balance(content.astype(str), 4500)

    self.texts.append(content['text'])
    labels = {'0': 0, '1': 1, '2': 2}
    self.labels.append(content['labels'].apply(lambda x: labels[x]))

    self.n_examples = len(self.labels[0].tolist())
    print(len(self.labels[0].tolist()))
    return

  def __len__(self):
    
    return self.n_examples

  def __getitem__(self, item):

    return {'text':self.texts[0].tolist()[item],
            'label':self.labels[0].tolist()[item]}

class Gpt2ClassificationCollator(object):
    def __init__(self, use_tokenizer, labels_encoder, max_sequence_len=None):
        self.use_tokenizer = use_tokenizer
        self.max_sequence_len = use_tokenizer.model_max_length if max_sequence_len is None else max_sequence_len
        self.labels_encoder = labels_encoder

        return

    def __call__(self, sequences):
        texts = [sequence['text'] for sequence in sequences]
        labels = [sequence['label'] for sequence in sequences]
        
        labels = [self.labels_encoder[str(label)] for label in labels]
        inputs = self.use_tokenizer(text=texts, return_tensors="pt", padding=True, truncation=True,  max_length=self.max_sequence_len)
        inputs.update({'labels':torch.tensor(labels)})

        return inputs


def train(dataloader, optimizer_, scheduler_, device_):
  global model

  predictions_labels = []
  true_labels = []
  total_loss = 0

  model.train()

  for batch in tqdm(dataloader, total=len(dataloader)):

    true_labels += batch['labels'].numpy().flatten().tolist()

    batch = {k:v.type(torch.long).to(device_) for k,v in batch.items()}
    
    model.zero_grad()
    outputs = model(**batch)
    loss, logits = outputs[:2]
    total_loss += loss.item()

    loss.backward()
    torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

    optimizer_.step()

    scheduler_.step()

    logits = logits.detach().cpu().numpy()

    predictions_labels += logits.argmax(axis=-1).flatten().tolist()

  avg_epoch_loss = total_loss / len(dataloader)
  
  return true_labels, predictions_labels, avg_epoch_loss


def validation(dataloader, device_):
  global model

  predictions_labels = []
  true_labels = []
  total_loss = 0

  model.eval()

  for batch in tqdm(dataloader, total=len(dataloader)):

    true_labels += batch['labels'].numpy().flatten().tolist()

    batch = {k:v.type(torch.long).to(device_) for k,v in batch.items()}

    with torch.no_grad():        

        outputs = model(**batch)

        loss, logits = outputs[:2]
        
        logits = logits.detach().cpu().numpy()

        total_loss += loss.item()
        
        predict_content = logits.argmax(axis=-1).flatten().tolist()

        predictions_labels += predict_content

  avg_epoch_loss = total_loss / len(dataloader)

  return true_labels, predictions_labels, avg_epoch_loss

print('Loading configuraiton...')
model_config = GPT2Config.from_pretrained(pretrained_model_name_or_path=model_name_or_path, num_labels=n_labels)

print('Loading tokenizer...')
tokenizer = GPT2Tokenizer.from_pretrained(pretrained_model_name_or_path=model_name_or_path)
tokenizer.padding_side = "left"
tokenizer.pad_token = tokenizer.eos_token

print('Loading model...')
model = GPT2ForSequenceClassification.from_pretrained(pretrained_model_name_or_path=model_name_or_path, config=model_config)

model.resize_token_embeddings(len(tokenizer))

model.config.pad_token_id = model.config.eos_token_id

model.to(device)
print('Model loaded to `%s`'%device)

gpt2_classificaiton_collator = Gpt2ClassificationCollator(use_tokenizer=tokenizer, 
                                                          labels_encoder=labels_ids, 
                                                          max_sequence_len=max_length)

print('Dealing with Train...')
train_dataset = ClassifierDataset(path='./datasets/twitter/train.csv')
print('Created `train_dataset` with %d examples!'%len(train_dataset))

train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, collate_fn=gpt2_classificaiton_collator)
print('Created `train_dataloader` with %d batches!'%len(train_dataloader))

print()

print('Dealing with Validation...')
valid_dataset =  ClassifierDataset(path='./datasets/twitter/test.csv')
print('Created `valid_dataset` with %d examples!'%len(valid_dataset))

valid_dataloader = DataLoader(valid_dataset, batch_size=batch_size, shuffle=False, collate_fn=gpt2_classificaiton_collator)
print('Created `eval_dataloader` with %d batches!'%len(valid_dataloader))

optimizer = torch.optim.AdamW(model.parameters(), lr = 2e-5)

total_steps = len(train_dataloader) * epochs

scheduler = get_linear_schedule_with_warmup(optimizer, 
                                            num_warmup_steps = 0,
                                            num_training_steps = total_steps)

all_loss = {'train_loss':[], 'val_loss':[]}
all_acc = {'train_acc':[], 'val_acc':[]}

print('Epoch')
for epoch in tqdm(range(epochs)):
  print()
  print('Training on batches...')
  train_labels, train_predict, train_loss = train(train_dataloader, optimizer, scheduler, device)
  train_acc = accuracy_score(train_labels, train_predict)
  print()
  print('Validation on batches...')
  valid_labels, valid_predict, val_loss = validation(valid_dataloader, device)
  val_acc = accuracy_score(valid_labels, valid_predict)

  print("  train_loss: %.5f - val_loss: %.5f - train_acc: %.5f - valid_acc: %.5f"%(train_loss, val_loss, train_acc, val_acc))
  print()

  all_loss['train_loss'].append(train_loss)
  all_loss['val_loss'].append(val_loss)
  all_acc['train_acc'].append(train_acc)
  all_acc['val_acc'].append(val_acc)

show_stats(all_acc, all_loss)

true_labels, predictions_labels, avg_epoch_loss = validation(valid_dataloader, device)

evaluation_report = classification_report(true_labels, predictions_labels, labels=list(labels_ids.values()), target_names=list(labels_ids.keys()))
print(evaluation_report)
