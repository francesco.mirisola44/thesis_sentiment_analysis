import torch
import numpy as np
from transformers import RobertaTokenizer, RobertaModel
from torch import nn
from torch.optim import Adam, AdamW
from tqdm import tqdm
from torch.utils.tensorboard import SummaryWriter
from labelled import *
from sklearn.utils import shuffle

tokenizer = RobertaTokenizer.from_pretrained('roberta-base')
labels = {'0':0, '1':1, '2': 2}

class Dataset(torch.utils.data.Dataset):
    def __init__(self, df):
        self.labels = [labels[label] for label in df['labels'].astype(str)]
        self.texts = [tokenizer(text, 
                               padding='max_length', max_length = 512, truncation=True,
                                return_tensors="pt") for text in df['text']]
    def classes(self):
        return self.labels

    def __len__(self):
        return len(self.labels)

    def get_batch_labels(self, idx):
        return np.array(self.labels[idx])

    def get_batch_texts(self, idx):
        return self.texts[idx]

    def __getitem__(self, idx):
        batch_texts = self.get_batch_texts(idx)
        batch_y = self.get_batch_labels(idx)
        return batch_texts, batch_y

class DatasetPred(torch.utils.data.Dataset):
    def __init__(self, df):
        self.texts = [tokenizer(text, padding='max_length', max_length=512, truncation=True, return_tensors="pt") for text in df['text']]

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        batch_texts = self.texts[idx]
        return batch_texts
    
class BertClassifier(nn.Module):
    def __init__(self, dropout):
        super(BertClassifier, self).__init__()
        self.bert = RobertaModel.from_pretrained('roberta-base')
        self.dropout = nn.Dropout(dropout)
        self.linear = nn.Linear(768, 3)
        self.relu = nn.ReLU()

    def forward(self, input_id, mask):
        _, pooled_output = self.bert(input_ids=input_id, attention_mask=mask, return_dict=False)
        dropout_output = self.dropout(pooled_output)
        linear_output = self.linear(dropout_output)
        final_layer = self.relu(linear_output)

        return final_layer
    
def train(model, train_data, val_data, learning_rate, epochs):
    writer = SummaryWriter('logs_training')
    train, val = Dataset(train_data), Dataset(val_data)

    train_dataloader = torch.utils.data.DataLoader(train, batch_size=16, shuffle=True)
    val_dataloader = torch.utils.data.DataLoader(val, batch_size=16)

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    criterion = nn.CrossEntropyLoss()
    optimizer = AdamW(model.parameters(), lr= learning_rate)

    if use_cuda:
        model = model.cuda()
        criterion = criterion.cuda()
    all_loss = {'train_loss':[], 'val_loss':[]}
    all_acc = {'train_acc':[], 'val_acc':[]}
    for epoch_num in range(0, epochs):
            total_acc_train = 0
            total_loss_train = 0

            for train_input, train_label in tqdm(train_dataloader):
                train_label = train_label.to(device)
                mask = train_input['attention_mask'].to(device)
                input_id = train_input['input_ids'].squeeze(1).to(device)

                output = model(input_id, mask)
                
                batch_loss = criterion(output, train_label.long())
                total_loss_train += batch_loss.item()
                
                acc = (output.argmax(dim=1) == train_label).sum().item()
                total_acc_train += acc

                model.zero_grad()
                batch_loss.backward()
                optimizer.step()
            
            total_acc_val = 0
            total_loss_val = 0

            with torch.no_grad():
                for val_input, val_label in val_dataloader:
                    val_label = val_label.to(device)
                    mask = val_input['attention_mask'].to(device)
                    input_id = val_input['input_ids'].squeeze(1).to(device)

                    output = model(input_id, mask)

                    batch_loss = criterion(output, val_label.long())
                    total_loss_val += batch_loss.item()
                    
                    acc = (output.argmax(dim=1) == val_label).sum().item()
                    total_acc_val += acc
            writer.add_scalar('Loss/Train', total_loss_train / len(train_data), epoch_num)
            writer.add_scalar('Accuracy/Train', total_acc_train / len(train_data), epoch_num)
            writer.add_scalar('Loss/Val', total_loss_val / len(val_data), epoch_num)
            writer.add_scalar('Accuracy/Val', total_acc_val / len(val_data), epoch_num)
            print(f'Epochs: {epoch_num + 1} | Train Loss: {total_loss_train / len(train_data): .3f} | Train Accuracy: {total_acc_train / len(train_data): .3f} | Val Loss: {total_loss_val / len(val_data): .3f} | Val Accuracy: {total_acc_val / len(val_data): .3f}')
            all_loss['train_loss'].append(total_loss_train )
            all_loss['val_loss'].append(total_loss_val)
            all_acc['train_acc'].append(total_acc_train)
            all_acc['val_acc'].append(total_acc_val)
    writer.close()
    return model

def evaluate(model, test_data):
    test = Dataset(test_data)

    test_dataloader = torch.utils.data.DataLoader(test, batch_size=4)

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    if use_cuda:
        model = model.cuda()

    total_acc_test = 0
    with torch.no_grad():
        for test_input, test_label in test_dataloader:
            test_label = test_label.to(device)
            mask = test_input['attention_mask'].to(device)
            input_id = test_input['input_ids'].squeeze(1).to(device)

            output = model(input_id, mask)

            acc = (output.argmax(dim=1) == test_label).sum().item()
            total_acc_test += acc
    print(f'Test Accuracy: {total_acc_test / len(test_data): .3f}')

def load_bert_classifier(model_path):
    tokenizer = RobertaTokenizer.from_pretrained('roberta-base')
    model = BertClassifier(0.2)
    model.load_state_dict(torch.load(model_path))
    model.eval()
    return model, tokenizer

def predict(model, tokenizer, sentence):
    inputs = tokenizer(sentence, padding='max_length', max_length=512, truncation=True, return_tensors="pt")
    input_ids = inputs['input_ids']
    attention_mask = inputs['attention_mask']

    input_ids = input_ids.squeeze(0)
    attention_mask = attention_mask.squeeze(0)

    data = DatasetPred(pd.DataFrame({'text': [sentence]}))

    dataloader = torch.utils.data.DataLoader(data, batch_size=4)

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    if use_cuda:
        model = model.cuda()

    category = 0
    probabilities_sentiments = []
    with torch.no_grad():
        for batch_texts in tqdm(dataloader):

            attention_mask = batch_texts['attention_mask'].to(device)
            input_ids = batch_texts['input_ids'].squeeze(1).to(device)

            output = model(input_ids, attention_mask)
            probabilities = torch.softmax(output, dim=1)
            predicted_label = torch.argmax(probabilities, dim=1).item()
            label_probs = probabilities.squeeze().tolist()

            label_names = ['0', '1', '2']

            category = label_names[predicted_label]
            for label, prob in zip(label_names, label_probs):
                probabilities_sentiments.append(prob)

    return category, probabilities_sentiments[0], probabilities_sentiments[1], probabilities_sentiments[2]

def balance(input_df, desired_samples_per_label):
    label_0_df = input_df[input_df['labels'] == '0']
    label_1_df = input_df[input_df['labels'] == '1']
    label_2_df = input_df[input_df['labels'] == '2']

    sampled_label_0_df = label_0_df.sample(desired_samples_per_label, replace=True)
    sampled_label_1_df = label_1_df.sample(desired_samples_per_label, replace=True)
    sampled_label_2_df = label_2_df.sample(desired_samples_per_label, replace=True)

    balanced_df = pd.concat([sampled_label_0_df, sampled_label_1_df, sampled_label_2_df])

    shuffled_balanced_df = shuffle(balanced_df)

    return shuffled_balanced_df

def main():
    df_csv = read_csv('./sentiment_db/general/train.csv')
    df_csv['labels'] = df_csv['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})

    df = balance(df_csv.astype(str), 1500)
    #df = df_csv.astype(str)
    np.random.seed(112)

    df_train, df_val, df_test = np.split(df.sample(frac=1, random_state=42), [int(.8*len(df)), int(.9*len(df))])
    #df_train = balance(df_csv.astype(str), 4500)
    #df_val = balance(df_csv.astype(str), 4500)
    print(len(df_train), len(df_val), len(df_test))
    EPOCHS = 10
    model = BertClassifier(0.4)
    LR = 1e-2

    model = train(model, df_train, df_val, LR, EPOCHS)

    df_test = read_csv('./sentiment_db/general/test.csv')
    df_test['labels'] = df_test['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    evaluate(model, df_test)
    torch.save(model.state_dict(), './roBERTa/transfer/model_final.pt')

    model_path = './roBERTa/transfer/model_final.pt'
    model, tokenizer = load_bert_classifier(model_path)
    print("Model and tokenizer loaded")

    print("Making predictions")
    
    df_csv = read_csv('./sentiment_db/general/train.csv')
    df_csv['labels'] = df_csv['sentiment'].replace({'neutral': '1', 'positive': '2', 'negative': '0'})
    df = df_csv.astype(str)

    for index, row in tqdm(df.iterrows(), total=df.shape[0]):
        category, prob_negative, prob_neutral, prob_positive = predict(model, tokenizer, row['description'])
        df.at[index, 'prediction'] = category
        df.at[index, 'Probability Negative'] = prob_negative
        df.at[index, 'Probability Neutral'] = prob_neutral
        df.at[index, 'Probability Positive'] = prob_positive

    df[['text', 'labels', 'prediction', 'Probability Negative', 'Probability Neutral', 'Probability Positive']].to_excel('db_predicted.xlsx', index=False)

if __name__ == '__main__':
    main()