import torch
from transformers import GPT2Config, GPT2Tokenizer, GPT2ForSequenceClassification

n_labels = 3
model_path = './gpt2model/model_final_gpt2.pt'
labels_ids = {'0': 0, '1': 1, '2': 2}

def load_gpt2():
    print('Loading configuraiton...')
    model_config = GPT2Config.from_pretrained(pretrained_model_name_or_path='gpt2', num_labels=n_labels)
    print('Loading model...')
    model = GPT2ForSequenceClassification.from_pretrained(pretrained_model_name_or_path=model_path, config=model_config)
    return model

def load_tokenizer():
    print('Loading tokenizer...')
    tokenizer = GPT2Tokenizer.from_pretrained(pretrained_model_name_or_path='gpt2')
    tokenizer.padding_side = "left"
    tokenizer.pad_token = tokenizer.eos_token
    return tokenizer

def get_probabilities(input_text, model, tokenizer):
    inputs = tokenizer(input_text, return_tensors='pt', padding=True, truncation=True, max_length=512)
    with torch.no_grad():
        logits = model(**inputs).logits
        probabilities = torch.softmax(logits, dim=-1)
    return probabilities

def predict_sentiment(probabilities):
    probabilities = probabilities.tolist()
    max_prob = max(probabilities)
    max_prob_index = probabilities.index(max_prob)
    predicted_class = list(labels_ids.keys())[max_prob_index]
    return str(predicted_class)